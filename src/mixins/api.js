
let verifiedSeeds;
let notVerifiedSeeds;

export default {
  methods: {
    async getVerifiedSeeds() {

      if (!verifiedSeeds) {
        const response = await this.$http({ method: "GET", url: "/api/verified" });
        verifiedSeeds = response.body;
      }

      return verifiedSeeds;
    },
    async getVerifiedAllTmpSeeds() {

      if (!verifiedSeeds) {
        const response = await this.$http({ method: "GET", url: "/api/verified?limit=10000&page=0" });
        verifiedSeeds = response.body;
      }

      return verifiedSeeds;
    },
    async getNotVerifiedSeeds() {

      if (!notVerifiedSeeds) {
        const response = await this.$http({ method: "GET", url: "/api/notverified" });
        notVerifiedSeeds = response.body;
      }

      return notVerifiedSeeds;
    },
    async getSeed(seedID) {

      const response = await this.$http({ method: "GET", url: "/api/seed/"+seedID });
      if (response.ok) {
        return response.body;
      }
      return null;
    },
    async getSPS() {

      const response = await this.$http({ method: "GET", url: "/api/sps" });
      if (response.ok) {
        return response.body;
      }
      return null;
    },
    async getTotalScanned() {

      const response = await this.$http({ method: "GET", url: "/api/totalscanned" });
      if (response.ok) {
        return response.body;
      }
      return null;
    }
  }
}
