import Vue from 'vue'
import store from './store'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Advanced from './views/Advanced.vue'
import About from './views/About.vue'
import Seed from './views/Seed.vue'
import Download from './views/Download.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/advanced',
      name: 'advanced',
      component: Advanced
    },
    {
      path: '/download',
      name: 'download',
      component: Download
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/seed/:seed',
      name: 'seed',
      component: Seed
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.claims.authorized) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router
