export default class ImgGen {
  async init() {
    const response = await fetch('/gen_image.wasm');
    const wasm = await response.arrayBuffer();
    const gen_image = await WebAssembly.compile(wasm);
    this.instance = await WebAssembly.instantiate(
      gen_image,
      {
        wasi_snapshot_preview1: { 
          fd_write(){},
          fd_close(){},
          fd_seek(){},
          proc_exit(){},
        }
      },
    );

    this.canvas = document.getElementById('c');
    this.ctx = this.canvas.getContext('2d');
    const pointer = this.instance.exports.get_buffer();
    const data = new Uint8ClampedArray(this.instance.exports.memory.buffer, pointer, 512 * 512 * 4);
    this.img = new ImageData(data, 512, 512);
  }

  gen(inputString) {
    const seed = BigInt(inputString);

    // extract low and high 32 bits because js ints are stupid
    const lo = Number(seed & BigInt(0xFFFFFFFF)) >>> 0;
    const hi = Math.floor(Number(seed / BigInt(4294967296))) >>> 0;

    this.instance.exports.gen_image(lo, hi);
    this.ctx.putImageData(this.img, 0, 0);
  }
}
